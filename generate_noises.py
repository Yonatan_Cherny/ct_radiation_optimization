from yc_optimization_funcs import optimize_sources_ftw, estimate_mse_empiric

from yc_astra_funcs import optimize_ml, mse, ScanGenerator, solve_lsqr_sparse
from yc_astra_funcs import crlb_mse, weak_crlb_mse, space_carve, undersample_obj
from progressbar import ProgressBar
import numpy as np
import scipy.io as io
import scipy as sp
from scipy import signal
import matplotlib.pyplot as plt
import itertools
import astra
from time import time
from sklearn.linear_model import LinearRegression

np.set_printoptions(precision=2)
# orig_ground = io.loadmat('../astra-1.8/samples/phantom.mat')['phantom256']*10
orig_ground = np.load('./phantom/slice_460.npy')
# orig_ground = orig_ground[90:260, 90:260]

# limits_coeffs = [[0.1, 0.9], [0.3, 0.7]]
orig_ground = orig_ground[100:250, 100:250]*10
plt.figure()
plt.imshow(orig_ground)
# plt.show()


# under_sampling_rate = 32
# under_sampling_rate = 16
# under_sampling_rate = 12
# under_sampling_rate = 8
# under_sampling_rate = 6
# under_sampling_rate = 4
# under_sampling_rate = 3
# under_sampling_rate = 2
# under_sampling_rate = 1.5
under_sampling_rate = 1

# ground = io.loadmat('./astra-1.8/samples/phantom.mat')['phantom256']*10#00
# ground = signal.convolve2d(np.ones([under_sampling_rate, under_sampling_rate])/under_sampling_rate**2,
#                            ground)[::under_sampling_rate,::under_sampling_rate]
# ground = ground[::under_sampling_rate,::under_sampling_rate]
undersampled_ground, index_mat = undersample_obj(orig_ground, under_sampling_rate)
print(f'Obejct dimesnion - {undersampled_ground.shape}')
# undersampled_ground[:,:] = 1#1e-5
# n_x, n_y = (20, 20)#(256,256)
# ground = np.random.rand(n_x,n_y)*2
# ground = np.zeros([n_x,n_y])
# ground[1:7,7:10] += 20.4
# ground[4:6,3:9] += 5.3
# ground[3:8, -4:-2] += 20
# ground[::3, -2:-1] += 10 #20
# ground[::3, 1:3] += 10 #20
# ground[6:11,10:15] += 3
# ground[8:10,12:14] += 1

orig_voxel_size = [2.3e-3]*2
x_size = orig_voxel_size[0]*orig_ground.shape[0]
x_vol_lim = [-x_size/2, x_size/2]

y_size = orig_voxel_size[1]*orig_ground.shape[1]
y_vol_lim = [-y_size/2, y_size/2]

print(y_vol_lim, x_vol_lim)
# x_vol_lim = np.array([-0.5,0.5])*3
# y_vol_lim = np.array([-0.5,0.5])*3

plt.figure()
plt.gray()
plt.imshow(undersampled_ground)
plt.colorbar()
# plt.show()
# _ = plt.xticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])
# _ = plt.yticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])

################################################################
pixel_width = 1e-3#1e-2#1e-2#5e-3
n_angles = 180#500#90#180#720
n_sensors = 150#150 #100
radiation_lvl = 1e6/pixel_width
source_dist = 1 #0.7#1.5
sensor_dist = 0.5#1.5
smooth_rad_filter_ma = 1
print(f'Sensor aperture - {pixel_width*n_sensors/(sensor_dist*np.pi*2)}')

empty_atten = undersampled_ground.min()
empty_atten_opt = None
print(undersampled_ground.min(), undersampled_ground.max())
################################################################
x_roi = [6, 12]
y_roi = [6, 12]

vox_sub_tuple = list(itertools.product(np.arange(6, 12), np.arange(6,12)))
vox_sub = [[],[]]
for tup in vox_sub_tuple:
    vox_sub[0].append(tup[0])
    vox_sub[1].append(tup[1])
# vox_sub = [np.arange(2,6), np.arange(8,12)]
# vox_sub = itertools.product(vox_sub)
# vox_list = None#np.ravel_multi_index(vox_sub, ground.shape)

################################################################
projection_model = ['strip', 'line'][0]
scan_gen = ScanGenerator(undersampled_ground, *undersampled_ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist, projection_model)


# TODO: make space_carve take into account the overall R in R_mat
# max_atten = empty_atten * np.linalg.norm(undersampled_ground.shape)

min_rad = radiation_lvl//3
reduced_W, non_empty_vox = space_carve(scan_gen.R_mat, scan_gen.generate_poisson(min_rad),
                                  min_rad, empty_atten, 1)

reduced_W, vox_list = space_carve(scan_gen.R_mat, scan_gen.attenuation,
                                    1, empty_atten, 0)
# vox_orig_list = np.ravel
# vox_list = np.concatenate([vox_list[19:24], vox_list[30:34]])
# vox_list = [vox_list[520]]
non_empty_sub = np.unravel_index(non_empty_vox, undersampled_ground.shape)
# plt.figure()
# plt.plot(non_empty_sub[1], non_empty_sub[0], '*')
# vox_list =

_, index_mat_under = undersample_obj(undersampled_ground, 7)
vox_list = tuple(index_mat_under[len(index_mat_under)//2 + 1*0, len(index_mat_under)//2 - 1])
# vox_list = non_empty_vox
# vox_list = [undersampled_ground.size//3 + 10]
# vox_sub = [[undersampled_ground.shape[0]//2 - 2], [undersampled_ground.shape[1]//2 - 3]]
vox_sub = np.unravel_index(vox_list, undersampled_ground.shape)

plt.plot(vox_sub[1] + 0.1, vox_sub[0] + 0.1, '.b', markersize=1.2)
path_save = './example_test_23_7.eps'
plt.savefig(path_save, format='eps')
plt.show()
# vox_list = np.arange(undersampl  ed_ground.size)

orig_ground_idx = index_mat[vox_sub].reshape(-1,)

# plt.figure()
# plt.imshow(orig_ground)
# plt.plot(*np.unravel_index(orig_ground_idx, orig_ground.shape), '*b')
# plt.show()
recon_method = ['lsqr', 'lsqr_space_carve', 'sirt'][0]
n_est = 5

# uniform_mse_list, uniform_out_list = estimate_mse_empiric(undersampled_ground, scan_gen, radiation_lvl,
#                                                           vox_list=vox_list, n_est=n_est,
#                                                           recon_method=recon_method)
mean_vals = undersampled_ground.mean()

noised_est = [undersampled_ground + np.random.randn(*undersampled_ground.shape) * 0.3*mean_vals for _ in range(n_est)]

for est in noised_est:
    plt.figure()
    plt.imshow(est.reshape(*undersampled_ground.shape))
    plt.show()