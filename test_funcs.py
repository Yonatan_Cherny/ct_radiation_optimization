from yc_astra_funcs import run_reconstruction_with_noise, crlb_mse, weak_crlb_mse, optimize_ml, mse
import numpy as np
import scipy.io as io
import matplotlib.pyplot as plt
import itertools
import astra
from time import time
from sklearn.linear_model import LinearRegression
from progressbar import ProgressBar

# ground = io.loadmat('./astra-1.8/samples/phantom.mat')['phantom256'][::8,::8]#*10
np.random.seed(170590)
n_x, n_y = (40, 40)#(256,256)
ground = np.random.rand(n_x,n_y)*1
# ground = np.zeros([n_x,n_y])
# ground[1:7,7:10] = 0.4
# ground[4:6,3:9] += 0.3

# x_vol_lim = [-0.5,0.5]
# y_vol_lim = [-0.5,0.5]
# pixel_width = 1e-2#1e-2#5e-3
# n_angles = 720#180
# n_sensors = 100
# radiation_lvl = 10**2
# source_dist = 3
# sensor_dist = 3


x_vol_lim = [-0.5,0.5]
y_vol_lim = [-0.5,0.5]
pixel_width = 2e-2#1e-2#5e-3
n_angles = 180#720
n_sensors = 50
radiation_lvl = 10**3
source_dist = 3
sensor_dist = 3



# plt.figure()
# plt.gray()
# plt.imshow(ground)

s_time_rec = time()
noisy_rec, clean_rec, W, noisy_meas, clean_meas = run_reconstruction_with_noise(ground, *ground.shape,
                                                                                x_vol_lim=x_vol_lim,
                                                                                y_vol_lim=y_vol_lim,
                                                                                pixel_width=pixel_width,
                                                                                n_angles=n_angles,
                                                                                n_sensors=n_sensors,
                                                                                radiation_lvl=radiation_lvl,
                                                                                source_dist=source_dist,
                                                                                sensor_dist=sensor_dist,
                                                                                return_measurements=True,
                                                                                n_iter=500)

# print(f'Basic reconstruction took {(time() - s_time_rec)/2} (twice)')

clean_err = noisy_rec - clean_rec
ground_err = noisy_rec - ground
init_err = clean_rec - ground

# print(clean_err.ravel().std(), init_err.ravel().std(), ground_err.ravel().std())
print(f'Error of noisy reconstruction - {mse(ground_err)}')
print(f'Error of clean reconstruction from ground - {mse(init_err)}')
print(f'Error from clean reconstruction - {mse(clean_err)}')

# print(W.shape, clean_meas.shape)
s_weak = time()
weak_bound = weak_crlb_mse(W, clean_meas, radiation_lvl)
t_weak = time() - s_weak

s_crlb = time()
strong_bound = crlb_mse(W, clean_meas, radiation_lvl)
t_crlb = time() - s_crlb
print(f'Strong bound - {strong_bound}, took - {t_crlb} Seconds')
print(f'Weak bound - {weak_bound}, took - {t_weak} Seconds')

# plt.figure(figsize=[20,10])
# plt.gray()
# plt.subplot(1,3,1)
# plt.imshow(ground)
# plt.colorbar()
# plt.subplot(1,3,2)
# plt.imshow(clean_rec)
# plt.colorbar()
# plt.title(mse(init_err))
# plt.subplot(1,3,3)
# plt.imshow(noisy_rec)
# plt.colorbar()
# plt.title(mse(ground_err))
#
# plt.figure(figsize=[20,10])
# plt.gray()
# plt.subplot(1,3,1)
# plt.imshow(init_err)
# plt.colorbar()
# plt.subplot(1,3,2)
# plt.imshow(clean_err)
# plt.colorbar()
# plt.title(mse(init_err))
# plt.subplot(1,3,3)
# plt.imshow(clean_err)
# plt.colorbar()
# plt.title(mse(ground_err))

vol_geom = astra.create_vol_geom(*ground.shape, x_vol_lim[0], x_vol_lim[1], y_vol_lim[0], y_vol_lim[1])
proj_geom = astra.create_proj_geom('fanflat', pixel_width, n_sensors, np.linspace(0, 2 * np.pi, n_angles, False),
                                   source_dist, sensor_dist)
# proj_id = astra.create_projector('strip_fanflat', proj_geom, vol_geom)
proj_id = astra.create_projector('line_fanflat', proj_geom, vol_geom)

meas_atten = noisy_meas/radiation_lvl
clean_atten = clean_meas/radiation_lvl

# ('CG', 400), ('TNC',2000), ('BFGS', 40), ('Newton-CG',50), ('SLSQP',2)
methods = [('L-BFGS-B', 1500)]#, 'trust-ncg', 'trust-krylov'] # 'Newton-CG' , 'dogleg'

# methods = [methods[2]]
f_tol = 1e-80
g_tol = 1e-30
for method, max_iter in methods:
    print('='*50)
    print(f'Using {method} optimization method on noisy')
    s_time = time()
    # try:
    init_mat = None
    # init_mat = noisy_rec
    opt_res = optimize_ml(proj_id, meas_atten, init_mat, max_iter=max_iter, meas_based_hessian=True, opt_method=method,
                          disp_converge=False, gtol=g_tol, ftol=f_tol)
    print(f'Log likelihood - {opt_res.fun} \n'
          f'Iterations - {opt_res.nit} \n'
          f'Objective n eval - {opt_res.nfev} \n'
          f'Message - {opt_res.message}')
    # print(opt)
    # except:
        # print('Optimization failed')
        # continue
    print(f'SART noisy error - {mse(noisy_rec-ground)}')
    print(f'MLE noisy error - {mse(opt_res.x.reshape(*ground.shape)-ground)}')
    print(f'Distance from SART - {mse(opt_res.x.reshape(*ground.shape)-noisy_rec)}')
    print(f'Recon Took {time() - s_time} Seconds')
    print('='*50)
    ##########
    print('='*50)
    print(f'Using {method} optimization method on clean')
    s_time = time()
    try:
        init_mat = None
        # init_mat = noisy_rec
        opt_res = optimize_ml(proj_id, clean_atten, init_mat, max_iter=max_iter, meas_based_hessian=False, opt_method=method,
                              disp_converge=False, gtol=g_tol, ftol=f_tol)
        print(f'Log likelihood - {opt_res.fun} \n'
              f'Iterations - {opt_res.nit} \n'
              f'Objective n eval - {opt_res.nfev} \n'
              f'Message - {opt_res.message}')
        # print(opt)
    except:
        print('Optimization failed')
        continue
    print(f'SART clean error - {mse(clean_rec-ground)}')
    print(f'MLE clean error - {mse(opt_res.x.reshape(*ground.shape)-ground)}')
    print(f'Distance from SART - {mse(opt_res.x.reshape(*ground.shape)-clean_rec)}')
    print(f'Recon Took {time() - s_time} Seconds')
    print('='*50)
# opt_res_clean = optimize_ml(proj_id, clean_atten, clean_rec, max_iter=100)
# print(mse(clean_rec-ground))
# print(mse(opt_res_clean.x.reshape(*ground.shape)-ground))
# print(mse(opt_res_clean.x.reshape(*ground.shape)-clean_rec))
print('Done!')
astra.data2d.clear()