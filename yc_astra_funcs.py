import astra
import numpy as np
import cupy as cp
import scipy
from scipy.optimize import minimize
from scipy.linalg import cholesky, solve_triangular
from scipy.sparse import diags
from time import time
from itertools import product

from geometry_funcs import compute_crlb_coeff


def run_reconstruction_with_noise(ground_truth, n_x, n_y, x_vol_lim, y_vol_lim, pixel_width, n_angles, n_sensors,
                                  radiation_lvl, source_dist, sensor_dist, return_measurements=False,
                                  reconstruction_alg='SART', thermal_noise_level=None, n_iter=20):

    vol_geom = astra.create_vol_geom(n_x, n_y, x_vol_lim[0], x_vol_lim[1], y_vol_lim[0], y_vol_lim[1])
    proj_geom = astra.create_proj_geom('fanflat', pixel_width, n_sensors, np.linspace(0, 2 * np.pi, n_angles, False),
                                       source_dist, sensor_dist)
    # proj_id = astra.create_projector('strip_fanflat', proj_geom, vol_geom)
    proj_id = astra.create_projector('line_fanflat', proj_geom, vol_geom)

    matrix_idx = astra.projector.matrix(proj_id)
    W = astra.matrix.get(matrix_idx)  #

    # print(f'Weight matrix shape - {W.shape}')
    # print(f'n_x - {n_x}, n_y - {n_y} => n_x * n_y = {n_x*n_y}')
    # print(f'n_angles - {n_angles}, n_sensors - {n_sensors} => n_angles * n_sensors = {n_angles*n_sensors}')

    sinogram_id, sinogram = astra.create_sino(ground_truth, proj_id)
    noisy_sinogram_id = astra.create_sino(ground_truth, proj_id, returnData=False)
    # print(sinogram_id, noisy_sinogram_id)
    attenuation = np.exp(-sinogram)

    if isinstance(radiation_lvl, np.ndarray):
        radiation_lvl = radiation_lvl[:, np.newaxis]

    photon_count = np.int64(attenuation*radiation_lvl)

    if thermal_noise_level is None:
        noisy_count = np.random.poisson(photon_count)
    else:
        noisy_count = photon_count + np.random.randn(*photon_count.shape) * thermal_noise_level

    noisy_count_norm = noisy_count / radiation_lvl
    # noisy_count_norm = noisy_count_norm.clip(min=0)  # TODO: understand why is there negative, poisson should be non-negative

    # print(max(noisy_count_norm.ravel()), min(noisy_count_norm.ravel()))
    noisy_sino = -np.log(noisy_count_norm)

    astra.data2d.store(noisy_sinogram_id, noisy_sino)

    # print('sinograms created!')

    rec_id = astra.data2d.create('-vol', vol_geom)
    noisy_rec_id = astra.data2d.create('-vol', vol_geom)

    cfg = astra.astra_dict(reconstruction_alg)
    cfg['ReconstructionDataId'] = rec_id
    cfg['ProjectionDataId'] = sinogram_id
    cfg['ProjectorId'] = proj_id

    noisy_cfg = astra.astra_dict(reconstruction_alg)
    noisy_cfg['ReconstructionDataId'] = noisy_rec_id
    noisy_cfg['ProjectionDataId'] = noisy_sinogram_id
    noisy_cfg['ProjectorId'] = proj_id

    # Available algorithms:
    # ART, SART, SIRT, CGLS, FBP


    # Create the algorithm object from the configuration structure
    alg_id = astra.algorithm.create(cfg)
    noisy_alg_id = astra.algorithm.create(noisy_cfg)

    # Run 20 iterations of the algorithm
    # This will have a runtime in the order of 10 seconds.
    s_recon = time()
    # print('reconstructing noisy')
    astra.algorithm.run(noisy_alg_id, n_iter)
    # print(f'Done - took {time() - s_recon} Seconds')
    s_recon = time()
    # print('reconstructing clean')
    astra.algorithm.run(alg_id, n_iter)
    # print('done reconstruction')
    # print(f'Done - took {time() - s_recon} Seconds')

    # Get the result
    clean_rec = astra.data2d.get(rec_id)
    noisy_rec = astra.data2d.get(noisy_rec_id)

    # Clean up.
    astra.algorithm.delete(alg_id)
    astra.algorithm.delete(noisy_alg_id)
    astra.data2d.delete(rec_id)
    astra.data2d.delete(noisy_rec_id)
    astra.data2d.delete(noisy_sinogram_id)
    astra.data2d.delete(sinogram_id)
    astra.projector.delete(proj_id)
    # astra.data2d.info()
    # astra.projector.info()
    # astra.algorithm.info()
    if return_measurements:
        return noisy_rec, clean_rec, W, noisy_count, photon_count
    else:
        return noisy_rec, clean_rec, W


def crlb_mse(weight_mat, measurement, rad_level):
    cov_bound = compute_crlb_coeff(weight_mat, measurement, rad_level)
    # print(f'CRLB cov bound shape - {cov_bound.shape}')
    mse_error = cov_bound.diagonal().mean()
    return mse_error


def compute_weak_crlb_coeff_diag(weight_mat, measurement, rad_level):

    if isinstance(rad_level, np.ndarray):
        radiation_lvl = rad_level[:, np.newaxis]

    h_mat = measurement.flatten()/rad_level
    h_mat_sqrt = np.sqrt(h_mat)
    delta_vec = weight_mat.T.multiply(h_mat_sqrt)#.todense()
    delta_sqr = delta_vec.power(2)
    weak_crlb_diag = delta_sqr.sum(axis=1)#.flatten().reshape(-1,1)
    # print(weak_crlb_diag.shape)
    return 1/weak_crlb_diag


def weak_crlb_mse(weight_mat, measurement, rad_level):
    weak_bound = compute_weak_crlb_coeff_diag(weight_mat, measurement, rad_level)
    # print(f'Weak CRLB diagonal- {weak_bound.shape}')
    return weak_bound.mean()/rad_level


def run_reconstruction_with_noise_ml(ground_truth, n_x, n_y, x_vol_lim, y_vol_lim, pixel_width, n_angles, n_sensors,
                                     radiation_lvl, source_dist, sensor_dist, thermal_noise_level=None, n_iter=20,
                                     init_step=1, init_mat=None):
    vol_geom = astra.create_vol_geom(n_x, n_y, x_vol_lim[0], x_vol_lim[1], y_vol_lim[0], y_vol_lim[1])
    proj_geom = astra.create_proj_geom('fanflat', pixel_width, n_sensors, np.linspace(0, 2 * np.pi, n_angles, False),
                                       source_dist, sensor_dist)
    # proj_id = astra.create_projector('strip_fanflat', proj_geom, vol_geom)
    proj_id = astra.create_projector('strip_fanflat', proj_geom, vol_geom)

    sinogram_id, sinogram = astra.create_sino(ground_truth, proj_id)
    # noisy_sinogram_id = astra.create_sino(ground_truth, proj_id, returnData=False)

    attenuation = np.exp(-sinogram)

    photon_count = np.int64(attenuation * radiation_lvl)
    if thermal_noise_level is None:
        noisy_count = np.random.poisson(photon_count)
    else:
        noisy_count = photon_count + np.random.randn(*photon_count.shape) * thermal_noise_level

    noisy_count_norm = noisy_count / radiation_lvl
    noisy_count_norm = noisy_count_norm.clip(min=0)
    #  TODO: understand why is there negative, poisson should be non-negative

    #     noisy_rec = ml_estimator_gd_with_proj_id(proj_id, attenuation, ground_truth,
    #                                              n_iter=n_iter, init_step=1)

    init_mat = init_mat if init_mat is not None else ground_truth
    noisy_rec = ml_estimator_gd_with_proj_id(proj_id, noisy_count_norm, init_mat,
                                             n_iter=n_iter, init_step=init_step)

    astra.data2d.delete(sinogram_id)
    astra.projector.delete(proj_id)

    return noisy_rec


def ml_estimator_gd_with_proj_id(proj_id, measured_attenuation, init_mu_mat=None, n_iter=1000, init_step=1e-3):
    initial_shape = init_mu_mat.shape
    curr_mu_mat = init_mu_mat.copy().reshape(-1, 1)
    measured_attenuation = measured_attenuation.reshape(-1, 1)
    #     sinogram_id, sinogram = astra.create_sino(init_mat, proj_id)
    matrix_idx = astra.projector.matrix(proj_id)
    R = astra.matrix.get(matrix_idx).T.todense()
    step_vec = init_step / np.arange(1, n_iter + 1)
    #     print(R.shape, measured_attenuation.shape)
    for i_step, step in enumerate(step_vec):

        gradient = get_gradient(R, curr_mu_mat, initial_shape, measured_attenuation, proj_id)

        curr_mu_mat = curr_mu_mat + gradient * step


        # init_mat = np.clip(init_mat, a_min=0)

    return init_mu_mat.reshape(*initial_shape)


# def get_likelihood(mu_mat, mu_mat_shape, meas_atten_mat, proj_id):
#     n_elm = np.prod(mu_mat.shape)
#     meas_atten_vec = meas_atten_mat.reshape(-1, 1)
#     sinogram_id, curr_sinogram = astra.create_sino(mu_mat.reshape(*mu_mat_shape), proj_id)
#     curr_sinogram = np.float64(curr_sinogram)
#     astra.data2d.delete(sinogram_id)
#     curr_atten = np.exp(-curr_sinogram.reshape(-1,1))
#     likelihood = -curr_atten.sum() + (np.log(curr_atten) * meas_atten_vec).sum() # + ln(x_i !).sum(), this is omitted
#                                                                                     # because it does not depend on
#                                                                                     # the mu_mat
#     # print(likelihood)
#     return likelihood/n_elm


def get_likelihood(mu_mat, mu_mat_shape, meas_atten_mat, proj_id):
    n_elm = np.prod(mu_mat.shape)
    meas_atten_vec = meas_atten_mat.reshape(-1, 1)
    sinogram_id, curr_sinogram = astra.create_sino(mu_mat.reshape(*mu_mat_shape), proj_id)
    curr_sinogram = np.float64(curr_sinogram)
    astra.data2d.delete(sinogram_id)
    curr_atten = np.exp(-curr_sinogram.reshape(-1,1))
    dist = np.linalg.norm(curr_atten - meas_atten_mat.flatten())**2
    return -dist/n_elm


def get_gradient(R, mu_mat, mu_mat_shape, measured_attenuation, proj_id):
    n_elm = np.prod(mu_mat.shape)
    sinogram_id, sinogram = astra.create_sino(mu_mat.reshape(*mu_mat_shape), proj_id)
    sinogram = np.float64(sinogram)
    astra.data2d.delete(sinogram_id)
    # tmp_sino = np.exp(-sinogram).reshape(-1, 1)
    tmp_attenuation = np.exp(-sinogram).reshape(-1, 1)
    atten_err = tmp_attenuation - measured_attenuation.reshape(-1,1)
    gradient = np.matmul(R, atten_err)  # R.multiply(atten_err)
    gradient = np.array(gradient).flatten()
    # print(gradient.max(), gradient.mean())
    return gradient/n_elm


def get_hessian(R_mat, meas_atten, reg_coeff=0.01):
    # happens to be that this is EXACTLY the fisher information
    hessian = compute_crlb_coeff(R_mat, meas_atten, rad_level=1, inv_flag=False) / R_mat.shape[1]
    hessian += np.eye(*hessian.shape) * reg_coeff
    flat_hessian = hessian.ravel()
    # print(flat_hessian.max(), flat_hessian.min(), flat_hessian.mean())
    return hessian


def optimize_ml(proj_id, measured_attenuation, init_mu_mat=None, disp_converge=True, max_iter=None,
                meas_based_hessian=True, opt_method='Newton-CG', gtol=1e-5, ftol=1e-10):

    if init_mu_mat is None:
        geometry = astra.projector.volume_geometry(proj_id)
        mu_mat_shape = [geometry['GridRowCount'], geometry['GridColCount']]
        init_mu_mat = np.zeros(mu_mat_shape)
    else:
        mu_mat_shape = init_mu_mat.shape

    options_dict = {'disp':disp_converge}
    options_dict['gtol'] = gtol
    options_dict['ftol'] = ftol
    if max_iter:
        options_dict['maxiter'] = max_iter

    mu_mat = np.array(init_mu_mat).copy()

    matrix_idx = astra.projector.matrix(proj_id)
    R = astra.matrix.get(matrix_idx).T.todense()

    objective_func = lambda x: -get_likelihood(x, mu_mat_shape, measured_attenuation, proj_id)
    gradient_func = lambda x: -get_gradient(R, x, mu_mat_shape, measured_attenuation, proj_id)
    if meas_based_hessian:
        meas_hessian = -1*get_hessian(R, measured_attenuation)
        hess_func = lambda x: meas_hessian
    else:
        sinogram_id, sinogram = astra.create_sino(mu_mat.reshape(*init_mu_mat.shape), proj_id)
        astra.data2d.delete(sinogram_id)
        tmp_attenuation = np.exp(-sinogram)
        hess_func = lambda x: -1*get_hessian(R, tmp_attenuation)

    opt_result = minimize(objective_func, mu_mat.flatten(), method=opt_method,
                          jac=gradient_func, options=options_dict)#, tol=1e-10)#, hess=hess_func)

    return opt_result


def mse(err_mat):
    return np.power(err_mat.ravel(),2).mean()


def inv_diag_by_chol(x):
    chol_x = cholesky(x)
    inv_chol_x = solve_triangular(chol_x, np.eye(len(x)))

    return np.power(inv_chol_x,2).sum(axis=1)


class ScanGenerator:
    def __init__(self, ground, n_x, n_y, x_vol_lim, y_vol_lim, pixel_width,
                 n_sensors, n_angles, source_dist, sensor_dist, proj_method='line'):

        vol_geom = astra.create_vol_geom(n_x, n_y, x_vol_lim[0], x_vol_lim[1], y_vol_lim[0], y_vol_lim[1])
        proj_geom = astra.create_proj_geom('fanflat', pixel_width, n_sensors,
                                           np.linspace(0, 2 * np.pi, n_angles, False),
                                           source_dist, sensor_dist)
        # proj_id = astra.create_projector('strip_fanflat', proj_geom, vol_geom)
        proj_id = astra.create_projector(f'{proj_method}_fanflat', proj_geom, vol_geom)

        matrix_idx = astra.projector.matrix(proj_id)
        self.R_mat = astra.matrix.get(matrix_idx)

        sinogram_id, sinogram = astra.create_sino(ground, proj_id)
        self.attenuation = np.exp(-np.float64(sinogram))

        astra.data2d.delete(sinogram_id)
        astra.projector.delete(proj_id)

    def generate_poisson(self, radiation_lvl):
        if isinstance(radiation_lvl, np.ndarray):
            if radiation_lvl.size == self.attenuation.shape[0]:
                radiation_lvl = expand_source_to_R(radiation_lvl, self.attenuation.shape[1])
            radiation_lvl = radiation_lvl.reshape(*self.attenuation.shape)

        photon_count = np.int64(np.multiply(self.attenuation, radiation_lvl))

        noisy_count = np.random.poisson(photon_count)
        # noisy_count_norm = noisy_count / radiation_lvl # we rather return the noisy count
        return noisy_count

    def generate_clean(self, radiation_lvl):
        if isinstance(radiation_lvl, np.ndarray):
            radiation_lvl = radiation_lvl.reshape(*self.attenuation.shape)

        photon_count = np.int64(np.multiply(self.attenuation, radiation_lvl))
        return photon_count

    def generate_thermal(self, radiation_lvl, thermal_noise_level):
        if isinstance(radiation_lvl, np.ndarray):
            radiation_lvl = radiation_lvl[:, np.newaxis]

        photon_count = np.int64(self.attenuation * radiation_lvl)
        noisy_count = photon_count + np.random.randn(*photon_count.shape) * thermal_noise_level
        return noisy_count


def space_carve(W, meas_mat, source_intensity, air_attenuation, sigma_cutoff=0, epsilon_r=0.2):
    """
    Splits Weight mat (the scan_gen.R_mat) and measurement to the respective angle scan
    :param W: Weight matrix (result of scan_gen.R_mat). shape - (n_angles x n_sensors,  n_voxels)
    :param meas_mat: Measurement matrix ( results of scan_gen.generate_clean/poisson or scan_gen.attenuation).
                    shape - (n_angles, n_sensors)
    :return: ??? TODO: fill it up
    """
    if isinstance(source_intensity, np.ndarray):
        if source_intensity.size == meas_mat.shape[0]:
            source_intensity = expand_source_to_R(source_intensity, meas_mat.shape[1])
        source_intensity = source_intensity.reshape(*meas_mat.shape)

    min_dist_threshold = W.max() * epsilon_r
    log_air_attenuation_mat = W*air_attenuation
    log_angle_air_attenuation = log_air_attenuation_mat.sum(axis=1).reshape(-1,1)
    atten_vec = np.divide((meas_mat + sigma_cutoff*np.sqrt(meas_mat)), source_intensity).reshape(-1,1)
    log_atten_vec = -1*np.log(atten_vec)
    direct_measurements = np.where((log_atten_vec < log_angle_air_attenuation).reshape(-1,1))[0]
    # TODO: instead of disabling all the voxels in the way only disable the ones with enough weight
    # direct_meas_W = W[direct_measurements]
    direct_meas_W = W[direct_measurements] > min_dist_threshold
    is_empty_voxels_count = direct_meas_W.sum(axis=0)
    is_empty_voxels = is_empty_voxels_count > 0
    non_empty_voxels = (~is_empty_voxels).nonzero()[1]

    reduced_W = W[:, non_empty_voxels]
    # TODO: the next line will help, but it will be problematic with the rest of the code currently
    # reduced_W = reduced_W[~direct_measurements,:]
    return reduced_W, non_empty_voxels


def space_carve_sirt(W, meas_mat, source_intensity, air_attenuation, coeff=4):
    """
    Splits Weight mat (the scan_gen.R_mat) and measurement to the respective angle scan
    :param W: Weight matrix (result of scan_gen.R_mat). shape - (n_angles x n_sensors,  n_voxels)
    :param meas_mat: Measurement matrix ( results of scan_gen.generate_clean/poisson or scan_gen.attenuation).
                    shape - (n_angles, n_sensors)
    :return: ??? TODO: fill it up
    """
    if isinstance(source_intensity, np.ndarray):
        if source_intensity.size == meas_mat.shape[0]:
            source_intensity = expand_source_to_R(source_intensity, meas_mat.shape[1])
        source_intensity = source_intensity.reshape(*meas_mat.shape)

    # sirt_reconstruction(wei)

    min_dist_threshold = W.max() * epsilon_r
    log_air_attenuation_mat = W*air_attenuation
    log_angle_air_attenuation = log_air_attenuation_mat.sum(axis=1).reshape(-1,1)
    atten_vec = np.divide((meas_mat + sigma_cutoff*np.sqrt(meas_mat)), source_intensity).reshape(-1,1)
    log_atten_vec = -1*np.log(atten_vec)
    direct_measurements = np.where((log_atten_vec < log_angle_air_attenuation).reshape(-1,1))[0]
    # TODO: instead of disabling all the voxels in the way only disable the ones with enough weight
    # direct_meas_W = W[direct_measurements]
    direct_meas_W = W[direct_measurements] > min_dist_threshold
    is_empty_voxels_count = direct_meas_W.sum(axis=0)
    is_empty_voxels = is_empty_voxels_count > 0
    non_empty_voxels = (~is_empty_voxels).nonzero()[1]

    reduced_W = W[:, non_empty_voxels]
    # TODO: the next line will help, but it will be problematic with the rest of the code currently
    # reduced_W = reduced_W[~direct_measurements,:]
    return reduced_W, non_empty_voxels



# @jit
def undersample_obj(object, undersample_rate):
    in_shape = np.array(object.shape)
    out_shape = in_shape // undersample_rate
    object = object[:out_shape[0] * undersample_rate, :out_shape[0] * undersample_rate]
    out_mat = np.zeros(out_shape)

    for i in range(undersample_rate):
        for j in range(undersample_rate):
            out_mat += object[i::undersample_rate, j::undersample_rate]

    index_lists = [[[] for _ in range(out_shape[0])] for _ in range(out_shape[1])]
    for i,j in product(*[np.arange(n) for n in out_shape]):
        i_slice = slice(i*undersample_rate, i*undersample_rate + undersample_rate)
        j_slice = slice(j*undersample_rate, j*undersample_rate + undersample_rate)
        cell_indexes = np.mgrid[j_slice, i_slice].reshape(2,-1)
        index_lists[i][j] = np.ravel_multi_index(cell_indexes, in_shape)

    return out_mat/undersample_rate, np.array(index_lists)


def expand_source_to_R(rad_vec, n_sensors):
    return np.repeat(rad_vec, n_sensors)


# @jit
def sirt_reconstruction(weight_mat, meas_mat, rad_level, ground, modify_by_meas=True, n_iter=10000, clip_min=1e-100,
                        use_cupy=True):
    """
    :param weight_mat: Weight matrix (result of scan_gen.R_mat). shape - (n_angles x n_sensors,  n_voxels)
    :param meas_mat: Measurement matrix ( results of scan_gen.generate_clean/poisson or scan_gen.attenuation).
                    shape - (n_angles, n_sensors)
    :param rad_level:
    :param modify_by_meas:
    :param n_iter:
    :param clip_min:
    :return:
    """
    minus_w = -weight_mat
    flat_noisy = meas_mat.reshape(-1, 1)
    if isinstance(rad_level, np.ndarray):
        if rad_level.size == meas_mat.shape[0]:
            rad_level = expand_source_to_R(rad_level, meas_mat.shape[1])
        rad_level = rad_level.reshape(*flat_noisy.shape)

    # TODO: handle rad_level[i]=0

    flat_meas = np.divide(flat_noisy, rad_level).clip(min=clip_min)

    log_meas = np.log(flat_meas)

    if modify_by_meas:
        # This option normalizes every measurement by it's estimated STD, this way all the measurements are
        # noisy with the same noise level.

        # the outer sqrt is because of the square coming from the least square
        coef_vec = np.sqrt(flat_noisy)#np.sqrt(np.divide(log_derive, atten_var))

        minus_w = minus_w.multiply(coef_vec)
        log_meas = np.multiply(log_meas, coef_vec)

    # solve with SIRT
    mu_rec_list = []
    build_s = time()

    r_mat_flat = np.array((1/minus_w.sum(axis=1)).flat)
    c_mat_flat = np.array((1/minus_w.sum(axis=0)).flat)
    r_mat = np.diagflat(r_mat_flat)
    c_mat = np.diagflat(c_mat_flat)
    r_mat_sparse = diags(r_mat_flat, 0)
    c_mat_sparse = diags(c_mat_flat, 0)

    # print(f'{c_mat.shape} {minus_w.T.shape} {r_mat.shape}')
    # print(f'{log_meas.shape}')
    # out_mat = c_mat @ minus_w.T @ r_mat
    out_mat = c_mat_sparse @ minus_w.T @ r_mat_sparse

    b_part = out_mat @ log_meas
    a_part = (out_mat @ minus_w - np.eye(minus_w.shape[1]))

    mu_vec = np.zeros((minus_w.shape[1], 1))
    if use_cupy:
        a_part = cp.array(a_part)
        b_part = cp.array(b_part)
        mu_vec = cp.array(mu_vec)
    build_e = time()

    # bar = ProgressBar()
    # for i in bar(range(n_iter)):
    for _ in range(n_iter):
        # print(f'{b_part.shape} - {a_part.T.shape} @ {mu_vec.shape}')
        mu_vec = b_part - a_part @ mu_vec
        if use_cupy:
            np_mu_vec = cp.asnumpy(mu_vec)
        else:
            np_mu_vec = mu_vec

        mu_rec_list.append(np_mu_vec.reshape(*ground.shape))

    # b_part = out_mat @ log_meas
    # a_part = (out_mat @ minus_w - np.eye(minus_w.shape[1]))
    # build_e = time()
    # mu_vec = np.zeros((minus_w.shape[1],1))
    # bar = ProgressBar()
    # for i in bar(range(n_iter)):
    #     # print(f'{b_part.shape} - {a_part.T.shape} @ {mu_vec.shape}')
    #     mu_vec = mu_vec + out_mat@ (log_meas - minus_w@mu_vec)
    #     mu_rec_list.append(mu_vec.reshape(*ground.shape))
    e_iter = time()
    # print(f'Building matrices took {build_e - build_s} Seconds')
    # print(f'Iterations took {e_iter - build_e} Seconds')

    out_recon = np_mu_vec.reshape(*ground.shape)
    return out_recon, mse(out_recon - ground)
    # return mu_vec.reshape(*ground.shape), mu_rec_list


    # lsq_out = scipy.sparse.linalg.lsqr(minus_w, log_meas)
    # out_mat = lsq_out[0].reshape(ground.shape)
    # return out_mat, mse(out_mat - ground)


def solve_lsqr_sparse(weight_mat, meas_mat, rad_level, ground=None, modify_by_meas=True, clip_min=1e-20, coef_vec=None,
                      use_cupy=True):
    if ground is None:
        ground = np.zeros(weight_mat.shape[1])

    minus_w = -weight_mat
    flat_noisy = meas_mat.reshape(-1, 1)
    if isinstance(rad_level, np.ndarray):
        if rad_level.size == meas_mat.shape[0]:
            rad_level = expand_source_to_R(rad_level, meas_mat.shape[1])
        rad_level = rad_level.reshape(*flat_noisy.shape)

    # TODO: handle rad_level[i]=0

    flat_meas = np.divide(flat_noisy, rad_level).clip(min=clip_min)

    log_meas = np.log(flat_meas)

    if modify_by_meas:
        # This option normalizes every measurement by it's estimated STD, this way all the measurements are
        # noisy with the same noise level.

        # the outer sqrt is because of the square coming from the least square
        if coef_vec is None:
            coef_vec = np.sqrt(flat_noisy)#np.sqrt(np.divide(log_derive, atten_var))
        minus_w = minus_w.multiply(coef_vec)
        log_meas = np.multiply(log_meas, coef_vec)
    
    if use_cupy:
        # minus_w = scipy.sparse.csr_matrix(minus_w)
        minus_w = cp.sparse.csr_matrix(minus_w)
        log_meas = cp.array(log_meas)

        lsq_out = (cp.asnumpy(cp.matmul(cp.linalg.inv((minus_w.T*minus_w).todense()) * minus_w.T, log_meas)), )
        # lsq_out = cpx.scipy.sparse.linalg.lsqr(minus_w, log_meas)
        # lsq_out = cp.
    else:
        lsq_out = scipy.sparse.linalg.lsqr(minus_w, log_meas)
    out_mat = lsq_out[0].reshape(ground.shape)
    return out_mat, mse(out_mat - ground)


def lsqr_with_space_carve(W, meas_mat, rad_level, ground, **kwargs):
    """
    Splits Weight mat (the scan_gen.R_mat) and measurement to the respective angle scan
    :param W: Weight matrix (result of scan_gen.R_mat). shape - (n_angles x n_sensors,  n_voxels)
    :param meas_mat: Measurement matrix ( results of scan_gen.generate_clean/poisson or scan_gen.attenuation).
                    shape - (n_angles, n_sensors)
    :return: ??? TODO: fill it up
    """
    if 'air_attenuation' in kwargs.keys():
        air_attenuation = kwargs['air_attenuation']
    else:
        air_attenuation = ground.min()

    _, non_empty_voxels = space_carve(W, meas_mat, rad_level, air_attenuation)

    # non_empty_voxels_ground_sub = np.where(ground > ground.min())
    # non_empty_voxels_ground = np.ravel_multi_index(non_empty_voxels_ground_sub, ground.shape)
    # false_empty = np.setdiff1d(non_empty_voxels_ground, non_empty_voxels)
    # false_empty_sub = np.unravel_index(false_empty, ground.shape)


    empty_voxels = np.setdiff1d(np.arange(ground.size), non_empty_voxels)
    if len(empty_voxels) == 0:
        return solve_lsqr_sparse(W, meas_mat, rad_level, ground)

    # Save all the non-empty voxels + a single empty voxel
    save_voxels = np.concatenate(([empty_voxels[0]], non_empty_voxels))
    reduced_W = W.tolil()[:, save_voxels]
    reduced_W[:, 0] = W[:, empty_voxels].sum(axis=1)

    # solve
    solve_out, _ = solve_lsqr_sparse(reduced_W, meas_mat, rad_level, **kwargs)

    solve_out_full_flatten = np.zeros(ground.size)
    solve_out_full_flatten[non_empty_voxels] = solve_out[1:]
    solve_out_full_flatten[empty_voxels] = solve_out[0]

    out_mat = solve_out_full_flatten.reshape(ground.shape)
    return out_mat, mse(out_mat - ground)
