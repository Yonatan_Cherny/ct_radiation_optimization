from yc_optimization_funcs import optimize_sources_ftw

from yc_astra_funcs import run_reconstruction_with_noise, compute_weak_crlb_coeff_diag
from geometry_funcs import compute_crlb_coeff
from yc_astra_funcs import optimize_ml, mse, ScanGenerator, solve_lsqr_sparse, sirt_reconstruction,\
    lsqr_with_space_carve
from yc_astra_funcs import crlb_mse, weak_crlb_mse, space_carve, undersample_obj
from progressbar import ProgressBar
import numpy as np
import scipy.io as io
import scipy as sp
from scipy import signal
import matplotlib.pyplot as plt
import itertools
import astra
from time import time
from sklearn.linear_model import LinearRegression
from progressbar import ProgressBar

# ground = io.loadmat('./astra-1.8/samples/phantom.mat')['phantom256']*10
orig_ground = np.load('./phantom/slice_460.npy')

# plt.figure()
# plt.imshow(orig_ground)
# plt.show()

# under_sampling_rate = 32
# under_sampling_rate = 16
# under_sampling_rate = 12
under_sampling_rate = 8
# under_sampling_rate = 4
# ground = io.loadmat('./astra-1.8/samples/phantom.mat')['phantom256']*10#00
# ground = signal.convolve2d(np.ones([under_sampling_rate, under_sampling_rate])/under_sampling_rate**2,
#                            ground)[::under_sampling_rate,::under_sampling_rate]
# ground = ground[::under_sampling_rate,::under_sampling_rate]
undersampled_ground, _ = undersample_obj(orig_ground*100, under_sampling_rate)
# undersampled_ground[:,:] = 1#1e-5
# n_x, n_y = (20, 20)#(256,256)
# ground = np.random.rand(n_x,n_y)*2
# ground = np.zeros([n_x,n_y])
# ground[1:7,7:10] += 20.4
# ground[4:6,3:9] += 5.3
# ground[3:8, -4:-2] += 20
# ground[::3, -2:-1] += 10 #20
# ground[::3, 1:3] += 10 #20
# ground[6:11,10:15] += 3
# ground[8:10,12:14] += 1
x_vol_lim = np.array([-0.5,0.5])*3
y_vol_lim = np.array([-0.5,0.5])*3

# plt.figure()
# plt.gray()
# plt.imshow(undersampled_ground)
# plt.colorbar()
# plt.show()
# _ = plt.xticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])
# _ = plt.yticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])

# path_save = './phantom_33x33_6_sep.eps'
# plt.savefig(path_save, format='eps')
################################################################
pixel_width = 2e-2#1e-2#1e-2#5e-3
n_angles = 180#500#90#180#720
n_sensors = 200#100
radiation_lvl = 1e8/pixel_width
source_dist = 3#1.5
sensor_dist = 3#1.5

# empty_atten = None
#################################################   ###############
vox_sub_tuple = list(itertools.product(np.arange(6, 12), np.arange(6,12)))
vox_sub = [[],[]]
for tup in vox_sub_tuple:
    vox_sub[0].append(tup[0])
    vox_sub[1].append(tup[1])
# vox_sub = [np.arange(2,6), np.arange(8,12)]
# vox_sub = itertools.product(vox_sub)
# vox_list = None#np.ravel_multi_index(vox_sub, ground.shape)

################################################################
scan_gen = ScanGenerator(undersampled_ground, *undersampled_ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist, 'line')

# TODO: make space_carve take into account the overall R in R_mat

s_quick = time()
quick_lsqr, _ = lsqr_with_space_carve(scan_gen.R_mat, scan_gen.generate_poisson(radiation_lvl),
                                      radiation_lvl, undersampled_ground, modify_by_meas=True,
                                      clip_min=1e-20)

s_full = time()
uniform_out, _ = solve_lsqr_sparse(scan_gen.R_mat, scan_gen.generate_poisson(radiation_lvl),
                                   radiation_lvl, undersampled_ground, modify_by_meas=True,
                                   clip_min=1e-20)
e_full = time()
sirt_out, sirt_list = sirt_reconstruction(scan_gen.R_mat, scan_gen.generate_poisson(radiation_lvl),
                                   radiation_lvl, undersampled_ground, n_iter=10000, modify_by_meas=True,
                                   clip_min=1e-20)
e_sirt = time()

print(f'sparse lsqr took {e_full - s_full} Seconds')
print(f'carved sparse lsqr took {s_full - s_quick} Seconds')
print(f'sirt took {e_sirt - e_full} Seconds')

# err_list = []
# for vec in sirt_list:
#     err_list.append(np.linalg.norm(vec - uniform_out))

# plt.figure()
# plt.semilogy(np.array(err_list)/np.linalg.norm(uniform_out),'.')
# plt.show()
# print(np.array(err_list))
print('='*50)
print('Full LSQR relative error:')
print(np.linalg.norm(uniform_out - undersampled_ground)/np.linalg.norm(undersampled_ground))
print('='*50)
print('Carved LSQR relative error:')
print(np.linalg.norm(quick_lsqr - undersampled_ground)/np.linalg.norm(undersampled_ground))
print('='*50)
print('Full SIRT relative error:')
print(np.linalg.norm(sirt_out - undersampled_ground)/np.linalg.norm(undersampled_ground))
print('huh')
