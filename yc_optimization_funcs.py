import numpy as np
import cupy as cp
import cupyx as cpx
import scipy as sp
from yc_astra_funcs import ScanGenerator, space_carve, solve_lsqr_sparse, mse, \
    lsqr_with_space_carve, sirt_reconstruction
from geometry_funcs import angles_iterator, compute_crlb_coeff
from progressbar import ProgressBar
import matplotlib.pyplot as plt
from calculus_funcs import CalcObj

from test_functions import compare_gradient_calc_to_empiric

from cached_property import cached_property
from memoized import memoized

use_cupy_glob = True


def gradient_by_sub_fishers(rad_vec, sub_fisher_list, vox_list=None, use_cupy=use_cupy_glob, inv_fisher=None):
    if use_cupy:
        if inv_fisher is None:
            full_fisher = fisher_from_subfishers_cupy(rad_vec, sub_fisher_list)
            full_fisher = full_fisher.reshape(*sub_fisher_list[0].shape)
            inv_fisher = cp.linalg.inv(full_fisher)
            # inv_fisher = cp.asnumpy(inv_fisher)
    else:
        # full_fisher = (sum([r * f for r, f in zip(rad_vec, sub_fisher_list)])).todense()
        full_fisher = fisher_from_subfishers(rad_vec, sub_fisher_list).reshape(*sub_fisher_list[0].shape)
        inv_fisher = np.linalg.inv(full_fisher)

    if vox_list is None:
        if False:#use_cupy:
            grad_list = [np.trace(cp.asnumpy(-inv_fisher * sub_fisher * inv_fisher))
                         for sub_fisher in sub_fishers_to_cp(sub_fisher_list)]
        else:
            grad_list = [np.trace(-inv_fisher @ sub_fisher @ inv_fisher) for sub_fisher in sub_fisher_list]
    else:
        vox_list = np.array(vox_list)
        if use_cupy:
            # inv_fisher_cp = cp.array(inv_fisher)
            inv_fisher_cp = inv_fisher

            # grad_list = [cp.asnumpy((-inv_fisher_cp @ cp.array(sub_fisher.todense()) @ inv_fisher_cp).diagonal()[vox_list].sum())
            #              for sub_fisher in sub_fisher_list]

            inv_fisher_cp_2 = -inv_fisher_cp @ inv_fisher_cp
            # grad_list = [
            #     cp.asnumpy(inv_fisher_cp_2 @ cp.array(sub_fisher.todense())).diagonal()[vox_list].sum()
            #     for sub_fisher in sub_fisher_list]

            grad_list = [
                cp.asnumpy(cp.multiply(inv_fisher_cp_2, cp.array(sub_fisher.todense())))[vox_list,:].sum()
                for sub_fisher in sub_fisher_list]

        else:
            grad_list = [(-inv_fisher @ sub_fisher @ inv_fisher).diagonal()[0, vox_list].sum()
                         for sub_fisher in sub_fisher_list]

    return np.array(grad_list)


def gradient_and_more_sub_fisher(rad_vec, sub_fisher_list, vox_list=None, inv_fisher=None):
    if inv_fisher is None:
        full_fisher = (sum([r * f for r, f in zip(rad_vec, sub_fisher_list)])).todense()
        inv_fisher = np.linalg.inv(full_fisher)

    half_mult_list = [sub_fisher @ inv_fisher for sub_fisher in sub_fisher_list]
    g_vec = np.array([np.trace(x) for x in half_mult_list])
    grad_mat_list = [inv_fisher @ x for x in half_mult_list]

    if vox_list is None:
        grad_list = [np.trace(x) for x in grad_mat_list]
    else:
        grad_list = [x.diagonal()[0, vox_list].sum() for x in grad_mat_list]

    return grad_list, grad_mat_list, g_vec


@memoized(hashable=False)
def build_cupy_stacked(sub_fisher_list):
    stacked_mat = sp.sparse.hstack(sub_fisher_list)
    stacked_mat = stacked_mat.reshape(-1, len(sub_fisher_list), order='F')
    return cpx.scipy.sparse.csr_matrix(stacked_mat)


@memoized(hashable=False)
def build_stacked(sub_fisher_list):
    stacked_mat = sp.sparse.hstack(sub_fisher_list)
    return stacked_mat.reshape(-1, len(sub_fisher_list), order='F')


def fisher_from_subfishers_cupy(rad_vec, sub_fisher_list):
    stacked_sub_fisher = build_cupy_stacked(sub_fisher_list)
    diag_vec = cpx.scipy.sparse.diags(rad_vec)
    radded_mat = (stacked_sub_fisher*diag_vec).sum(axis=1)
    # radded_mat = stacked_sub_fisher*cp.array(rad_vec)
    return radded_mat


def fisher_from_subfishers(rad_vec, sub_fisher_list):
    stacked_sub_fisher = build_stacked(sub_fisher_list)
    diag_vec = sp.sparse.diags(rad_vec)
    radded_mat = (stacked_sub_fisher*diag_vec).sum(axis=1)
    # radded_mat = stacked_sub_fisher*cp.array(rad_vec)
    return radded_mat


def optimization_metric_sub_fisher(rad_vec, sub_fisher_list, vox_list=None, use_cupy=use_cupy_glob,
                                   return_full=False):
    if use_cupy:
        full_fisher = fisher_from_subfishers_cupy(rad_vec, sub_fisher_list)
        full_fisher = full_fisher.reshape(*sub_fisher_list[0].shape)
        inv_fisher = cp.linalg.inv(full_fisher)
        # inv_fisher = cp.asnumpy(inv_fisher)
    else:
        # full_fisher = (sum([r * f for r, f in zip(rad_vec, sub_fisher_list)])).todense()
        full_fisher = fisher_from_subfishers(rad_vec, sub_fisher_list).reshape(*sub_fisher_list[0].shape)
        inv_fisher = np.linalg.inv(full_fisher)

    if vox_list is None:
        metric = np.trace(inv_fisher)
    else:
        vox_list = np.array(vox_list)
        if use_cupy:
            metric = cp.asnumpy(inv_fisher.diagonal()[vox_list].sum())
        else:
            metric = inv_fisher.diagonal()[0, vox_list].sum()
            # metric = inv_fisher.diagonal()[vox_list].sum()
    if not return_full:
        return metric
    else:
        return metric, inv_fisher


def calculate_inv_from_diff_grad(a_inv, ai_b_ai, g):
    return a_inv - ai_b_ai/(1+g)


def emperic_derivative(foo, init_a=0, epsilon=1):
    return (foo(init_a + epsilon) - foo(init_a - epsilon)) / (2*epsilon)


def line_search(foo, init_x, gx, init_step, n_iters, eps=1e-3):
    step = init_step

    last_foo = foo(init_x)
    curr_foo = foo(init_x + gx * init_step)

    iter_counter = 0
    #     print(init_x + gx*step, curr_foo)
    while curr_foo < last_foo:
        step *= 2
        last_foo = curr_foo
        try:
            curr_foo = foo(init_x + gx * step)
        except:
            break
    # print(iter_counter)
    left_bound = init_x + gx * step / 4
    right_bound = init_x + gx * step
    for _ in range(n_iters):
        curr_x = (left_bound + right_bound) / 2
        #         print(curr_x, (left_bound, curr_x, right_bound))
        curr_derivative = emperic_derivative(lambda x: foo(curr_x + x), eps)
        if curr_derivative < 0:
            left_bound = curr_x
        elif curr_derivative > 0:
            right_bound = curr_x
        else:
            print('grad = 0!')
            break
    return curr_x


def optimize_sources_ftw(ground, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist,
                         proj_method, mean_radiation_level, min_radiation,
                         vox_list, n_iter=20, space_carve_empty_atten=None):
    scan_gen = ScanGenerator(ground, *ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                             n_sensors, n_angles, source_dist, sensor_dist, proj_method)

    voxel_mask = np.zeros(ground.size, dtype='bool')
    if space_carve_empty_atten is not None:
        reduced_scan_gen = ScanGenerator(ground, *ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                             n_sensors, n_angles, source_dist, sensor_dist, proj_method)

        reduced_W, inv_voxels = space_carve(scan_gen.R_mat, scan_gen.generate_poisson(min_radiation),
                                            min_radiation, space_carve_empty_atten, 0)
        # reduced_W, inv_voxels = space_carve_cheat(scan_gen.R_mat, ground, space_carve_empty_atten)
        voxel_mask[inv_voxels] = True
        if vox_list is not None:
            reduced_vox_list = [np.flatnonzero(inv_voxels == vox)[0] for vox in vox_list if vox in inv_voxels]
            show_reduced_vox_list = inv_voxels[reduced_vox_list]
        else:
            reduced_vox_list = vox_list
            show_reduced_vox_list = vox_list
        print(f'number of voxels reduced from {scan_gen.R_mat.shape[1]} to {reduced_W.shape[1]}')
        reduced_scan_gen.R_mat = reduced_W

        if vox_list is not None:
            print(f'old vox list length - {len(vox_list)}, new vox list length - {len(reduced_vox_list)}')
            plt.figure()
            plt.gray()
            plt.imshow(ground)
            reduced_vox_sub = np.unravel_index(show_reduced_vox_list, ground.shape)
            vox_sub = np.unravel_index(vox_list, ground.shape)
            plt.plot(reduced_vox_sub[1], reduced_vox_sub[0], '.r')
            plt.plot(vox_sub[1] + 0.1, vox_sub[0] + 0.1, '.b')
            plt.show()
    else:
        reduced_scan_gen = scan_gen
        reduced_vox_list = vox_list
        voxel_mask[:] = True

    preliminary_meas_mat = scan_gen.generate_poisson(min_radiation)
    reduced_scan_gen = scan_gen  # TODO: debug
    angle_gen = angles_iterator(reduced_scan_gen.R_mat, preliminary_meas_mat / min_radiation, voxel_mask)
    angle_fisher_list = []
    for sub_w, meas in angle_gen:
        angle_fisher_list.append(sp.sparse.csr_matrix(compute_crlb_coeff(sub_w, meas, 1, inv_flag=False)))

    rad_absorbed_coeff = 1 - preliminary_meas_mat.mean(axis=1) / min_radiation
    # print(rad_absorbed_coeff)
    # rad_absorbed_coeff[::2] = 1e-3
    # rad_absorbed_coeff[1::2] = 1e-2
    # rad_absorbed_coeff = None
    rad_vec, metric_vec = optimize_from_sub_fishers(angle_fisher_list, mean_radiation_level, min_radiation,
                                                    reduced_vox_list, n_iter, condition_vec=rad_absorbed_coeff)

    return rad_vec, metric_vec, scan_gen


def estimate_mse_empiric(ground, scan_gen, radiation_vec, vox_list=None, n_est=10, recon_method='lsqr'):

    if vox_list is None:
        vox_list = np.arange(ground.size)

    recon_funcs_dict = {'lsqr_space_carve': lsqr_with_space_carve,
                        'lsqr': solve_lsqr_sparse,
                        'sirt':sirt_reconstruction}

    recon_func = recon_funcs_dict[recon_method]

    opt_mse_list = []
    rec_list = []
    bar = ProgressBar()
    for i_est in bar(range(n_est)):
        opt_out, _ = recon_func(scan_gen.R_mat, scan_gen.generate_poisson(radiation_vec),
                                radiation_vec, ground, modify_by_meas=True,
                                clip_min=1e-20)
        # opt_out = opt_out.clip(min=0)
        opt_mse_list.append(mse((opt_out - ground).reshape(-1,1)[list(vox_list)]))
        rec_list.append(opt_out)

    return opt_mse_list, rec_list


def optimize_from_sub_fishers(sub_fisher_list, mean_radiation_level, min_radiation,
                              vox_list=None, n_iter=5, init_step_ratio=4e1, condition_vec=None,
                              n_line_search=0):
    # init_step_ratio = 1e-4
    # condition_vec = None
    n_angles = len(sub_fisher_list)

    if condition_vec is None:
        condition_vec = np.ones(n_angles)/n_angles

    condition_vec = condition_vec/condition_vec.sum()
    gradient_correction_mat = np.eye(n_angles)/condition_vec/n_angles - condition_vec
    curr_rad_vec = mean_radiation_level * np.ones(n_angles)
    init_step = mean_radiation_level * init_step_ratio

    calc_obj = CalcObj(sub_fisher_list, vox_list, use_cupy=use_cupy_glob)

    def metric_fun(rad_vec, return_full=False):
        return calc_obj.est_mse(rad_vec, return_full=return_full)

    cost_list = []

    bar = ProgressBar()
    for i in bar(range(n_iter)):
        i += 1
        step = init_step / i
        curr_cost_func, full_inv_fisher = metric_fun(curr_rad_vec, True)
        cost_list.append(curr_cost_func)

        # print(curr_cost_func, metric_fun(curr_rad_vec))

        raw_grad = calc_obj.gradient(curr_rad_vec, inv_fisher=full_inv_fisher) / \
                   curr_cost_func * mean_radiation_level

        full_grad = raw_grad @ gradient_correction_mat
        # full_grad = raw_grad
        if i <= n_line_search:
            curr_rad_vec = line_search(metric_fun, curr_rad_vec, -full_grad, step/20, 20)
        else:
            curr_rad_vec -= step*full_grad
        # print(np.linalg.norm(curr_rad_vec - old_curr_vec)/np.linalg.norm(full_grad)/ mean_radiation_level)
        # curr_rad_vec = curr_rad_vec.clip(min=min_radiation)
        # curr_rad_vec = curr_rad_vec / curr_rad_vec.mean() * mean_radiation_level
        curr_rad_vec = project_parrallel_to_min_value(curr_rad_vec, condition_vec,
                                                      min_radiation)
        # print(np.dot(condition_vec, curr_rad_vec))
        # print([curr_rad_vec.min(), min_radiation])


    curr_cost_func = metric_fun(curr_rad_vec)
    cost_list.append(curr_cost_func)
    return curr_rad_vec, cost_list


def project_parrallel_to_min_value(point, projection_vec, min_value, eps=1e-2):
    """
    Assume that all projection_vec elements are positive
    """
    critical_elements = point <= min_value * (1 + eps)
    dists_from_condition = (min_value - point).clip(min=0)
    overall_critical_dist = np.dot(dists_from_condition, projection_vec)

    free_projection_vec = projection_vec.copy()
    free_projection_vec[critical_elements] = 0
    free_projection_coeff = -overall_critical_dist/np.dot(free_projection_vec, free_projection_vec)

    out_point = point + dists_from_condition
    out_point = out_point + free_projection_coeff*free_projection_vec

    return out_point