from yc_optimization_funcs import optimize_sources_ftw, estimate_mse_empiric

from yc_astra_funcs import run_reconstruction_with_noise, compute_weak_crlb_coeff_diag
from geometry_funcs import compute_crlb_coeff
from yc_astra_funcs import optimize_ml, mse, ScanGenerator, solve_lsqr_sparse
from yc_astra_funcs import crlb_mse, weak_crlb_mse, space_carve, undersample_obj
from progressbar import ProgressBar
import numpy as np
import os
import scipy.io as io
import scipy as sp
from scipy import signal
import matplotlib.pyplot as plt
import itertools
import astra
from time import time
from sklearn.linear_model import LinearRegression

np.set_printoptions(precision=2)
# orig_ground = io.loadmat('../astra-1.8/samples/phantom.mat')['phantom256']*10
phantoms_list = ['./phantom/slice_460.npy',
                 './phantom/atten_coeff_v2.pkl']
orig_ground = np.load(phantoms_list[1], allow_pickle=True)[:,:,0]
save_dir = './knee_left_roi_v2'
print(f'Run name = {save_dir}')
os.makedirs(save_dir, exist_ok=True)
# orig_ground = orig_ground[90:260, 90:260]
modify_phantom=False
if modify_phantom:
    # limits_coeffs = [[0.1, 0.9], [0.3, 0.7]]
    orig_ground = orig_ground[100:250, 100:250]*20#*100
    # orig_ground = orig_ground[30:-30, 30:-30]*10
    # orig_ground[60:100, 20:30] += orig_ground.max()*1#orig_ground.max()*200
    orig_ground[60:100, 15:30] += orig_ground.max()*0#orig_ground.max()*200
    # orig_ground[20:30, 60:100] += 100
plt.figure()
plt.imshow(orig_ground)
# plt.show()


# under_sampling_rate = 32
# under_sampling_rate = 16
# under_sampling_rate = 12
# under_sampling_rate = 8
# under_sampling_rate = 6
# under_sampling_rate = 4
# under_sampling_rate = 3
# under_sampling_rate = 2
# under_sampling_rate = 1.5
under_sampling_rate = 1

# ground = io.loadmat('./astra-1.8/samples/phantom.mat')['phantom256']*10#00
# ground = signal.convolve2d(np.ones([under_sampling_rate, under_sampling_rate])/under_sampling_rate**2,
#                            ground)[::under_sampling_rate,::under_sampling_rate]
# ground = ground[::under_sampling_rate,::under_sampling_rate]
undersampled_ground, index_mat = undersample_obj(orig_ground, under_sampling_rate)
print(f'Obejct dimesnion - {undersampled_ground.shape}')
# undersampled_ground[:,:] = 1#1e-5
# n_x, n_y = (20, 20)#(256,256)
# ground = np.random.rand(n_x,n_y)*2
# ground = np.zeros([n_x,n_y])
# ground[1:7,7:10] += 20.4
# ground[4:6,3:9] += 5.3
# ground[3:8, -4:-2] += 20
# ground[::3, -2:-1] += 10 #20
# ground[::3, 1:3] += 10 #20
# ground[6:11,10:15] += 3
# ground[8:10,12:14] += 1

orig_voxel_size = [2.3e-3]*2
x_size = orig_voxel_size[0]*orig_ground.shape[0]
x_vol_lim = [-x_size/2, x_size/2]

y_size = orig_voxel_size[1]*orig_ground.shape[1]
y_vol_lim = [-y_size/2, y_size/2]

print(y_vol_lim, x_vol_lim)
# x_vol_lim = np.array([-0.5,0.5])*3
# y_vol_lim = np.array([-0.5,0.5])*3

plt.figure()
plt.gray()
plt.imshow(undersampled_ground)
plt.colorbar()
# plt.show()
# _ = plt.xticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])
# _ = plt.yticks(np.arange(len(undersampled_ground))[::8], np.linspace(x_vol_lim[0], x_vol_lim[1], len(undersampled_ground))[::8])

################################################################
pixel_width = 2e-3#2.3e-3#1e-2#1e-2#5e-3
n_angles = 180#180#500#90#180#720
n_sensors = 80#150 #100
radiation_lvl = 1e7/pixel_width
source_dist = 0.5#1.5
sensor_dist = 0.5#1.5
smooth_rad_filter_ma = 1
print(f'Sensor aperture - {pixel_width*n_sensors/(sensor_dist*np.pi*2)}')


empty_atten = undersampled_ground.min()
empty_atten_opt = None
print(undersampled_ground.min(), undersampled_ground.max())
################################################################
x_roi = [6, 12]
y_roi = [6, 12]

vox_sub_tuple = list(itertools.product(np.arange(6, 12), np.arange(6,12)))
vox_sub = [[],[]]
for tup in vox_sub_tuple:
    vox_sub[0].append(tup[0])
    vox_sub[1].append(tup[1])
# vox_sub = [np.arange(2,6), np.arange(8,12)]
# vox_sub = itertools.product(vox_sub)
# vox_list = None#np.ravel_multi_index(vox_sub, ground.shape)

################################################################
projection_model = ['strip', 'line'][0]
scan_gen = ScanGenerator(undersampled_ground, *undersampled_ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist, projection_model)


# TODO: make space_carve take into account the overall R in R_mat
# max_atten = empty_atten * np.linalg.norm(undersampled_ground.shape)

min_rad = radiation_lvl//3
reduced_W, non_empty_vox = space_carve(scan_gen.R_mat, scan_gen.generate_poisson(min_rad),
                                  min_rad, empty_atten, 1)

reduced_W, vox_list = space_carve(scan_gen.R_mat, scan_gen.attenuation,
                                    1, empty_atten, 0)
# vox_orig_list = np.ravel
# vox_list = np.concatenate([vox_list[19:24], vox_list[30:34]])
# vox_list = [vox_list[520]]
non_empty_sub = np.unravel_index(non_empty_vox, undersampled_ground.shape)
# plt.figure()
# plt.plot(non_empty_sub[1], non_empty_sub[0], '*')
# vox_list =

_, index_mat_under = undersample_obj(undersampled_ground, 8)
vox_list = tuple(index_mat_under[len(index_mat_under)//2 - 2, len(index_mat_under)//2 - 0])
# vox_list = non_empty_vox
# vox_list = [undersampled_ground.size//3 + 10]
# vox_sub = [[undersampled_ground.shape[0]//2 - 2], [undersampled_ground.shape[1]//2 - 3]]
vox_sub = np.unravel_index(vox_list, undersampled_ground.shape)

plt.plot(vox_sub[1] + 0.1, vox_sub[0] + 0.1, '.b', markersize=1.2)
path_save = os.path.join(save_dir, 'example_test_23_7.eps')
plt.savefig(path_save, format='eps')
plt.show()
# vox_list = np.arange(undersampl  ed_ground.size)

orig_ground_idx = index_mat[vox_sub].reshape(-1,)

# plt.figure()
# plt.imshow(orig_ground)
# plt.plot(*np.unravel_index(orig_ground_idx, orig_ground.shape), '*b')
# plt.show()

################################################################
# Check single reconstruction
recon_check = False

recon, mse_err = solve_lsqr_sparse(scan_gen.R_mat, scan_gen.generate_poisson(radiation_lvl),
                                   radiation_lvl, undersampled_ground, modify_by_meas=True,
                                   clip_min=1e-20)
plt.figure()
plt.imshow(recon)
plt.title('Reconstruction instance')
plt.show()
if recon_check:
    raise InterruptedError('recon_check is on, only checking a reconstruction')
################################################################
# empty_atten = None
n_iter = 15#15
opt_rad, cost_vec, scan_gen = \
    optimize_sources_ftw(undersampled_ground, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist, 'strip', radiation_lvl,
                         min_rad, vox_list, n_iter=n_iter, space_carve_empty_atten=empty_atten_opt)
print('='*50)
print('bound improvement')
print(cost_vec[-1]/len(vox_list), cost_vec[0]/len(vox_list),
      (cost_vec[0] - cost_vec[-1])/cost_vec[0])

plt.figure()
plt.plot(np.array(cost_vec)/len(vox_list), '.b')
plt.show()

if smooth_rad_filter_ma > 1:
    filter = np.zeros_like(opt_rad)
    filter[:smooth_rad_filter_ma] = 1/smooth_rad_filter_ma
    opt_rad = np.real(np.fft.ifft(np.fft.fft(opt_rad) * np.fft.fft(filter)))

recon_method = ['lsqr', 'lsqr_space_carve', 'sirt'][0]
n_est = 50
opt_mse_list, opt_out_list = estimate_mse_empiric(undersampled_ground, scan_gen, opt_rad, vox_list=vox_list,
                                                  n_est=n_est, recon_method=recon_method)
uniform_mse_list, uniform_out_list = estimate_mse_empiric(undersampled_ground, scan_gen, radiation_lvl,
                                                          vox_list=vox_list, n_est=n_est,
                                                          recon_method=recon_method)
opt_mse = np.mean(opt_mse_list)
opt_med = np.median(opt_mse_list)
uniform_mse = np.mean(uniform_mse_list)
uniform_med = np.median(uniform_mse_list)

plt.figure()
plt.title('Optimized reconstruction')
plt.imshow(opt_out_list[0].reshape(undersampled_ground.shape))
plt.colorbar()
plt.show()

path_save = os.path.join(save_dir, 'optimized_recon_undersamplesd_2_6_april.eps')
plt.savefig(path_save, format='eps')
plt.savefig(path_save.replace('eps', 'png'))

plt.figure()
plt.title('Uniform')
plt.imshow(uniform_out_list[0].reshape(undersampled_ground.shape))
plt.colorbar()
plt.show()

path_save = os.path.join(save_dir, 'uniform_recon_undersamplesd_test_23_7.eps')
plt.savefig(path_save, format='eps')
plt.savefig(path_save.replace( 'eps', 'png'))

print('='*50)
print('Mean MSE improvement')
print(opt_mse, uniform_mse, (uniform_mse - opt_mse)/uniform_mse)
print('='*50)
print('Median MSE improvement')
print(opt_med, uniform_med, (uniform_med - opt_med)/uniform_med)

plt.figure()
plt.boxplot([opt_mse_list, uniform_mse_list], labels=['Optimzied', 'Uniform'],
            showmeans=True)
plt.axhline(cost_vec[0]/len(vox_list), color='b', label='Uniform bound')
plt.axhline(cost_vec[-1]/len(vox_list), color='r', label='Optimized bound')
plt.legend()

path_save = os.path.join(save_dir, 'boxplot_mse_undersampled_test_23_7.eps')
plt.savefig(path_save, format='eps')
plt.savefig(path_save.replace('eps', 'png'))
plt.show()
##################################################################

plt.figure()
_ = plt.hist(opt_rad)
plt.axvline(opt_rad.mean(), c='r')

angle_grid = np.linspace(0.5 * np.pi, 2.5 * np.pi, n_angles, False)
plt.figure()
plt.polar(angle_grid, opt_rad, marker='.', label='optimized')
plt.polar(angle_grid, radiation_lvl * np.ones(n_angles), marker='.', label='Original')
plt.legend()

path_save = os.path.join(save_dir, 'radiation_profile_XSCAT_test_23_7.eps')
plt.savefig(path_save, format='eps')
plt.savefig(path_save.replace('eps', 'png'))
plt.show()

plt.figure()
plt.plot(opt_rad, '.', label='optimized')
# plt.plot(np.ones_like(opt_rad), '.', label='uniform')
plt.axhline(radiation_lvl, label='mean rad')
plt.axhline(min_rad, label='min rad')
plt.legend()
plt.show()

path_save = os.path.join(save_dir, 'optimized_rad_undersampled_test_23_7.eps')
plt.savefig(path_save, format='eps')
plt.savefig(path_save.replace( 'eps', 'png'))

np.savez(os.path.join(save_dir, 'reconstructions'), optimized=opt_out_list, uniform=uniform_out_list,
         ground=undersampled_ground, vox_roi=vox_sub,
         y_lims=y_vol_lim, x_vol_lim=x_vol_lim)
np.savez(os.path.join(save_dir, 'mse_stats'), optimized=opt_mse_list, uniform=uniform_mse_list, rms_est=cost_vec)
np.savez(os.path.join(save_dir,'projection_patterns'), angle_grid=angle_grid,
         optimized=opt_rad, uniform=radiation_lvl * np.ones(n_angles))

np.save(os.path.join(save_dir, 'scan_generator'), scan_gen)
np.savez(os.path.join(save_dir, 'setup'), pixel_width=pixel_width, n_angles=n_angles, n_sensors=n_sensors,
         radiation_lvl=radiation_lvl, source_dist=source_dist, sensor_dist=sensor_dist, min_rad=min_rad)
print('done')


# ################
# # full reconstruction
# full_scan_gen = ScanGenerator(orig_ground, *orig_ground.shape, x_vol_lim, y_vol_lim, pixel_width,
#                               n_sensors, n_angles, source_dist, sensor_dist, projection_model)
#
# # full_uniform_lb = crlb_mse(full_scan_gen.R_mat, full_scan_gen.generate_poisson(radiation_lvl), radiation_lvl)
# # full_opt_lb = crlb_mse(full_scan_gen.R_mat, full_scan_gen.generate_poisson(opt_rad), opt_rad)
#
# plt.figure()
# plt.imshow(orig_ground)
# plt.plot(*np.unravel_index(orig_ground_idx, orig_ground.shape), '*')
# plt.show()
#
# recon_method = 'sirt'
# n_est = 10
# opt_mse_list_full, opt_out_list_full = estimate_mse_empiric(orig_ground, full_scan_gen, opt_rad,
#                                                             vox_list=orig_ground_idx, n_est=n_est,
#                                                             recon_method=recon_method)
# uniform_mse_list_full, uniform_out_list_full = estimate_mse_empiric(orig_ground, full_scan_gen, radiation_lvl,
#                                                                     vox_list=orig_ground_idx, n_est=n_est,
#                                                                     recon_method=recon_method)
#
# plt.figure()
# plt.title('Optimized reconstruction')
# plt.imshow(opt_out_list_full[0].reshape(orig_ground.shape))
# plt.colorbar()
#
# plt.figure()
# plt.title('Uniform')
# plt.imshow(uniform_out_list_full[0].reshape(orig_ground.shape))
# plt.colorbar()
#
# opt_mse_full = np.mean(opt_mse_list_full)
# opt_med_full = np.median(opt_mse_list_full)
# uniform_mse_full = np.mean(uniform_mse_list_full)
# uniform_med_full = np.median(uniform_mse_list_full)
#
#
# print('='*50)
# print('Mean MSE improvement')
# print(opt_mse_full, uniform_mse_full, (uniform_mse_full - opt_mse_full)/uniform_mse_full)
# print('='*50)
# print('Median MSE improvement')
# print(opt_med_full, uniform_med_full, (uniform_med_full - opt_med_full)/uniform_med_full)
#
# plt.figure()
# plt.boxplot([opt_mse_list_full, uniform_mse_list_full])
# plt.axhline(uniform_mse_full, color='b', label='uniform MSE mean')
# plt.axhline(opt_mse_full, color='r', label='optimized MSE mean')
# # plt.axhline(full_uniform_lb, c='k', label='Uniform LB')
# # plt.axhline(full_opt_lb, c='k', label='optimized LB')
# plt.legend()
# plt.show()
#
# print('omg')