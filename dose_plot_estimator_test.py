from yc_optimization_funcs import optimize_sources_ftw, estimate_mse_empiric

from yc_astra_funcs import run_reconstruction_with_noise, compute_weak_crlb_coeff_diag
from geometry_funcs import compute_crlb_coeff
from yc_astra_funcs import optimize_ml, mse, ScanGenerator, solve_lsqr_sparse
from yc_astra_funcs import crlb_mse, weak_crlb_mse, space_carve, undersample_obj
from progressbar import ProgressBar
import numpy as np
import scipy.io as io
import scipy as sp
from scipy import signal
import matplotlib.pyplot as plt
import itertools

np.set_printoptions(precision=2)
orig_ground = np.load('./phantom/slice_460.npy')
# orig_ground = orig_ground[90:260, 90:260]

# limits_coeffs = [[0.1, 0.9], [0.3, 0.7]]
orig_ground = orig_ground[100:250, 100:250]
plt.figure()
plt.imshow(orig_ground)

under_sampling_rate = 2

undersampled_ground, index_mat = undersample_obj(orig_ground, under_sampling_rate)
print(f'Obejct dimesnion - {undersampled_ground.shape}')


orig_voxel_size = [2.3e-3]*2
x_size = orig_voxel_size[0]*orig_ground.shape[0]/2
x_vol_lim = [-x_size/2, x_size/2]

y_size = orig_voxel_size[1]*orig_ground.shape[1]
y_vol_lim = [-y_size/2, y_size/2]

print(y_vol_lim, x_vol_lim)

plt.figure()
plt.gray()
plt.imshow(undersampled_ground)
plt.colorbar()

################################################################
pixel_width = 3.5e-3#1e-2#1e-2#5e-3
n_angles = 180#500#90#180#720
n_sensors = 150#150 #100
radiation_lvl = 1e4/pixel_width
source_dist = 1 #0.7#1.5
sensor_dist = 0.5#1.5
smooth_rad_filter_ma = 1
print(f'Sensor aperture - {pixel_width*n_sensors/(sensor_dist*np.pi*2)}')


################################################################
x_roi = [6, 12]
y_roi = [6, 12]

vox_sub_tuple = list(itertools.product(np.arange(6, 12), np.arange(6,12)))
vox_sub = [[],[]]
for tup in vox_sub_tuple:
    vox_sub[0].append(tup[0])
    vox_sub[1].append(tup[1])

_, index_mat_under = undersample_obj(undersampled_ground, 14)
vox_list = tuple(index_mat_under[len(index_mat_under)//2 + 1*0, len(index_mat_under)//2 - 1])

################################################################
projection_model = ['strip', 'line'][0]
scan_gen = ScanGenerator(undersampled_ground, *undersampled_ground.shape, x_vol_lim, y_vol_lim, pixel_width,
                         n_sensors, n_angles, source_dist, sensor_dist, projection_model)


################################################################
run_sim = False
raw_est = compute_crlb_coeff(scan_gen.R_mat, scan_gen.attenuation, 1).diagonal()[0,vox_list].mean()
if run_sim:
    recon_method = ['lsqr', 'lsqr_space_carve', 'sirt'][0]
    n_est = 50
    rad_vec = radiation_lvl * np.logspace(0, 2, 15)
    # bar = ProgressBar()
    mse_list_list = []
    for rad in rad_vec:
        uniform_mse_list, _ = estimate_mse_empiric(undersampled_ground, scan_gen, rad,
                                                  vox_list=vox_list, n_est=n_est,
                                                  recon_method=recon_method)
        mse_list_list.append(uniform_mse_list)

    np.savez('dose_analysis', stats_list=mse_list_list,
             rad_list=rad_vec, rmse_est=raw_est / rad_vec)
else:
    res = np.load('dose_analysis.npz')
    mse_list_list = res['stats_list']
    rad_vec = np.array(res['rad_list'])


raw_est = compute_crlb_coeff(scan_gen.R_mat, scan_gen.attenuation, 1).diagonal()[0,vox_list].mean()
mse_vec = np.array([np.mean(mse_list) for mse_list in mse_list_list])

plt.figure()
plt.plot(rad_vec, mse_vec, '.', label='Empiric RMSE')
plt.title('RMSE vs Radiation')
plt.xlabel('Radiation Dose [photons/pixel]')
plt.ylabel('RMSE [1/cm]')
plt.legend()
plt.savefig('./dose_sim_plot_clean_empirical.jpeg', format='jpeg')

plt.figure()
plt.plot(rad_vec, mse_vec, '.', label='Empiric RMSE')
plt.plot(rad_vec, raw_est / rad_vec, '-', label='Estimator')
plt.title('RMSE vs Radiation')
plt.xlabel('Radiation Dose [photons/pixel]')
plt.ylabel('RMSE [1/cm]')
plt.legend()

plt.savefig('./dose_sim_plot_clean_both.jpeg', format='jpeg')
plt.show()


plt.figure()
plt.boxplot(np.array(mse_list_list).T, positions=rad_vec, showmeans=True)
plt.plot(rad_vec, raw_est / rad_vec, '-')
plt.xlabel('Radiation Dose [photons/pixel]')
plt.ylabel('RMSE [1/cm]')
# plt.ticklabel_format(style='sci', useMathText=True)
# plt.tick_params()
plt.savefig('./dose_sim_plot_boxplot.png', format='png')
plt.show()