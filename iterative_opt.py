import astra
import numpy as np


class VolObj:
    def __init__(self, mu_mat_shape, x_vol_lim, y_vol_lim):
        self.mu_mat_shape = mu_mat_shape
        self.x_vol_lim = x_vol_lim
        self.y_vol_lim = y_vol_lim

        self.vol_geom = astra.create_vol_geom(*self.mu_mat_shape, *self.x_vol_lim, *self.y_vol_lim)


class SetupParameters:
    def __init__(self, pixel_width, n_angles, n_sensors, radiation_lvl, source_dist, sensor_dist,
                 proj_geom='fanflat', proj_method='line_fanflat'):
        self.pixel_width = pixel_width
        self.n_angles = n_angles
        self.n_sensors = n_sensors
        self.radiation_lvl = radiation_lvl
        self.source_dist = source_dist
        self.sensor_dist = sensor_dist

        self.proj_geom = proj_geom
        self.proj_method = proj_method

        self.proj_id = astra.create_proj_geom(self.proj_geom, self.pixel_width, self.n_sensors,
                                                np.linspace(0, 2 * np.pi, self.n_angles, False),
                                                self.source_dist, self.sensor_dist)


class ReconParameters:
    def __init__(self, reconstruction_alg, sing_n_iter, recon_n_iter):
        self.reconstruction_alg = reconstruction_alg
        self.single_n_iter = sing_n_iter
        self.recon_n_iter = recon_n_iter


class MetaOptimize:
    def __init__(self,
                 mu_mat_shape, x_vol_lim, y_vol_lim, # volume parameters
                pixel_width, n_angles, n_sensors, # machine sensing parameters 1
                radiation_lvl, source_dist, sensor_dist, # machine sensing parameters 2
                reconstruction_alg='SART', single_n_iter=20, recon_n_iter=20): # reconstruction parameters

        self.vol = VolObj(mu_mat_shape, x_vol_lim, y_vol_lim)
        self.setup = SetupParameters(pixel_width, n_angles, n_sensors, radiation_lvl, source_dist, sensor_dist)
        self.recon = ReconParameters(reconstruction_alg, single_n_iter, recon_n_iter)

        self.clean_sinogram_id = None
        self.noisy_sinogram_id = None

    def generate_sinograms_from_mu_mat(self, mu_mat):
        proj_id = self.setup.proj_id
        rad_level = self.setup.radiation_lvl

        self.clean_sinogram_id, clean_sinogram = astra.create_sino(mu_mat, proj_id)
        self.noisy_sinogram_id = astra.create_sino(mu_mat, proj_id, returnData=False)

        noisy_count = self.noise_sinogram(clean_sinogram, rad_level)

        noisy_sino = self.sinogram_from_meas(noisy_count, rad_level)

        astra.data2d.store(self.noisy_sinogram_id, noisy_sino)
        return self.clean_sinogram_id, self.noisy_sinogram_id

    def reconstruct_from_sino(self, sinogram_id):

        rec_id = astra.data2d.create('-vol', *self.vol.mu_mat_shape)

        cfg = astra.astra_dict(self.recon.reconstruction_alg)
        cfg['ReconstructionDataId'] = rec_id
        cfg['ProjectionDataId'] = sinogram_id
        cfg['ProjectorId'] = self.setup.proj_id

        alg_id = astra.algorithm.create(cfg)
        astra.algorithm.run(alg_id, self.recon.single_n_iter)
        reconstruction = astra.data2d.get(rec_id)

        astra.algorithm.delete(alg_id)
        astra.data2d.delete(rec_id)

        return reconstruction

    def iterative2_reconstruction(self, ground_mu_mat):
        _ = self.generate_sinograms_from_mu_mat(ground_mu_mat)

        recon_mat = reconstruct_from_meas()


    @staticmethod
    def noise_sinogram(sinogram, rad_level):
        attenuation = np.exp(-sinogram)
        photon_count = np.int64(attenuation * rad_level)
        noisy_count = np.random.poisson(photon_count)
        return noisy_count

    @staticmethod
    def sinogram_from_meas(measurement, rad_level):
        attenuation = measurement / rad_level
        sinogram = -np.log(attenuation)
        return sinogram














def reconstruct_from_meas(meas_mat, rad_level, proj_id, vol_geom, n_iter=1000, reconstruction_alg='SART'):

    mu_shape = [vol_geom['GridColCount'],vol_geom['GridRowCount']]

    # matrix_idx = astra.projector.matrix(proj_id)
    # W = astra.matrix.get(matrix_idx)  #

    # print(f'Weight matrix shape - {W.shape}')
    # print(f'n_x - {n_x}, n_y - {n_y} => n_x * n_y = {n_x*n_y}')
    # print(f'n_angles - {n_angles}, n_sensors - {n_sensors} => n_angles * n_sensors = {n_angles*n_sensors}')

    sinogram_id = astra.create_sino(np.zeros(mu_shape), proj_id, returnData=False)

    meas_atten_mat = meas_mat / rad_level

    meas_sino_mat = -np.log(meas_atten_mat)

    astra.data2d.store(sinogram_id, meas_sino_mat)

    # print('sinograms created!')

    rec_id = astra.data2d.create('-vol', vol_geom)
    # noisy_rec_id = astra.data2d.create('-vol', vol_geom)

    cfg = astra.astra_dict(reconstruction_alg)
    cfg['ReconstructionDataId'] = rec_id
    cfg['ProjectionDataId'] = sinogram_id
    cfg['ProjectorId'] = proj_id

    # Available algorithms:
    # ART, SART, SIRT, CGLS, FBP


    # Create the algorithm object from the configuration structure
    alg_id = astra.algorithm.create(cfg)
    # Run 20 iterations of the algorithm
    # This will have a runtime in the order of 10 seconds.
    print('reconstructing clean')
    astra.algorithm.run(alg_id, n_iter)
    print('done reconstruction')

    # Get the result
    mu_mat = astra.data2d.get(rec_id)

    # Clean up.
    astra.algorithm.delete(alg_id)
    astra.data2d.delete(rec_id)
    astra.data2d.delete(sinogram_id)
    astra.projector.delete(proj_id)

    return mu_mat


def measurement_from_mu_mat(mu_mat, proj_id, rad_level=1):
    sinogram_id, sinogram = astra.create_sino(mu_mat, proj_id)

    attenuation_mat =  np.exp(-sinogram)*rad_level

    astra.data2d.delete(sinogram_id)
    return attenuation_mat