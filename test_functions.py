import numpy as np
from scipy import sparse
# from yc_optimization_funcs import


def compare_gradient_calc_to_empiric(foo, goo, x0, eps=1e-4):
    empiric_grad = np.array([(foo(x0 + eps * ei) - foo(x0))/eps for ei in unit_iterator(len(x0))])
    calc_grad = goo(x0)

    return np.isclose(empiric_grad, calc_grad), empiric_grad, calc_grad


def unit_vec(n, i):
    vec = np.zeros(n)
    vec[i] = 1
    return vec


def unit_iterator(n):
    for i in range(n):
        yield unit_vec(n, i)


def generate_sub_fishers_list(n_vars, n_matrices, seed=170590):
    np.random.seed(seed)
    #TODO: generate covariance matrices using sp.sparse.random and actually calculate matrix. Now only using random
    # PSD matrices instead.
    return [generate_single_sub_fisher(n_vars) for _ in range(n_matrices)]


def generate_single_sub_fisher(n_vars, density=0.1):
    noise_mat = (sparse.random(n_vars, n_vars, density))*0.2
    diag_mat = (np.random.rand(1)[0] + 1) * sparse.eye(n_vars)
    return noise_mat + noise_mat.T + diag_mat