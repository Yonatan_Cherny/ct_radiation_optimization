import numpy as np
import cupy as cp
import cupyx as cpx
import scipy as sp
from time import time
from itertools import combinations, product

# from yc_astra_funcs import ScanGenerator, angles_iterator, compute_crlb_coeff, space_carve, solve_lsqr_sparse, mse, \
#     lsqr_with_space_carve, sirt_reconstruction
# from progressbar import ProgressBar
# import matplotlib.pyplot as plt


class CalcObj:
    def __init__(self, sub_fisher_list, vox_list=None, use_cupy=True):
        self.subfisher_list = sub_fisher_list
        if vox_list is None:
            self.vox_arr = np.arange(self._nvoxels)
        else:
            self.vox_arr = np.array(vox_list)

        self.vox_mask = np.zeros(self._nvoxels)
        self.vox_mask[self.vox_arr] = 1
        self.cupy_flag = use_cupy

        if self.cupy_flag:
            import cupy as cp
            import cupyx as cpx
            self.stacked_subfishers = self.build_cupy_stacked(sub_fisher_list)
            self.diag_vox_mask = cp.diag(self.vox_mask)
            self.vox_mask = cp.array(self.vox_mask)
        else:
            stacked_mat = sp.sparse.hstack(sub_fisher_list)
            self.stacked_subfishers = stacked_mat.reshape(-1, len(sub_fisher_list), order='F')
            self.diag_vox_mask = sp.sparse.diags(self.vox_mask)

    def fisher_mat(self, rad_vec):
        if self.cupy_flag:
            diag_vec = cpx.scipy.sparse.diags(rad_vec)
        else:
            diag_vec = sp.sparse.diags(rad_vec)

        out_fisher = (self.stacked_subfishers * diag_vec).sum(axis=1).reshape(self._nvoxels, self._nvoxels)
        return out_fisher

    def bound_mat(self, rad_vec):
        fisher_mat = self.fisher_mat(rad_vec)
        if self.cupy_flag:
            return cp.linalg.inv(fisher_mat)
        else:
            return np.linalg.inv(fisher_mat)

    def est_mse(self, rad_vec, return_full=False):
        inv_fisher = self.bound_mat(rad_vec)

        if self.cupy_flag:
            metric = cp.asnumpy(inv_fisher.diagonal()[self.vox_arr].sum())
        else:
            metric = inv_fisher.diagonal()[0, self.vox_arr].sum()

        if return_full:
            return metric, inv_fisher
        else:
            return metric

    def gradient(self, rad_vec, inv_fisher=None):
        if inv_fisher is None:
            inv_fisher = self.bound_mat(rad_vec)
        masked_inv_fisher = inv_fisher * self.vox_mask

        inv_fisher_2 = -masked_inv_fisher @ masked_inv_fisher.T
        # inv_fisher_2 = -inv_fisher @ self.diag_vox_mask @ inv_fisher
        #TODO: Currently irrelevant. Can be boosted by calculating only the relevant rows/columns of inv_fisher_2 instead of the whole
        # matrix and only then choosing the relevant rows/columns. shouldn't be hard.
        if self.cupy_flag:
            grad_list = [
                cp.multiply(inv_fisher_2, cp.array(sub_fisher.todense())).sum().item()
                for sub_fisher in self.subfisher_list]
        else:
            grad_list = [
                np.multiply(inv_fisher_2, np.array(sub_fisher.todense())).sum()
                for sub_fisher in self.subfisher_list]
        return np.array(grad_list)
    
    def hessian(self, rad_vec, inv_fisher=None, inv_fisher_2=None):
        if inv_fisher is None:
            inv_fisher = self.bound_mat(rad_vec)

        if inv_fisher_2 is None:
            inv_fisher_2 = -inv_fisher @ inv_fisher

        if not hasattr(self, 'sub_fishers_cov_mesh'):
            self.calc_sub_fisher_cov_mesh()

        inv_fisher_3 = -inv_fisher_2 @ inv_fisher  # note that the result is actualt inv_fisher^3 (not minus!)
        hess_mat = np.ndarray()
        if self.cupy_flag:
            hess_mat
            grad_list = [
                cp.multiply(inv_fisher_2, cp.array(sub_fisher.todense()))[self.vox_arr, :].sum().item()
                for sub_fisher in self.subfisher_list]
        else:
            grad_list = [
                np.multiply(inv_fisher_2, np.array(sub_fisher.todense()))[self.vox_arr, :].sum()
                for sub_fisher in self.subfisher_list]

    @property
    def _nvoxels(self):
        return self.subfisher_list[0].shape[0]

    @property
    def _nangles(self):
        return len(self.subfisher_list)

    @staticmethod
    def build_cupy_stacked(sub_fisher_list):
        stacked_mat = sp.sparse.hstack(sub_fisher_list)
        stacked_mat = stacked_mat.reshape(-1, len(sub_fisher_list), order='F')
        return cpx.scipy.sparse.csr_matrix(stacked_mat)

    @staticmethod
    def build_stacked(sub_fisher_list):
        stacked_mat = sp.sparse.hstack(sub_fisher_list)
        return stacked_mat.reshape(-1, len(sub_fisher_list), order='F')


if __name__ == '__main__':
    from test_functions import compare_gradient_calc_to_empiric
    from test_functions import generate_sub_fishers_list

    # test gradient
    n_subs = 50
    n_vars = 400
    vox_list = [10,20,15,30,45]
    fishers_test = generate_sub_fishers_list(n_vars, n_subs)
    calc_me = CalcObj(sub_fisher_list=fishers_test, vox_list=vox_list)
    rad_vec = np.random.rand(len(fishers_test))
    success = np.all(compare_gradient_calc_to_empiric(calc_me.est_mse, calc_me.gradient, rad_vec)[0])
    s=time()
    calc_me.gradient(rad_vec)
    print(f'Gradient calculation took - {time() - s} Seconds')
    if success:
        print('Gradient test passed!')
    else:
        print('FAILED')

