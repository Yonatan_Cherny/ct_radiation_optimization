import numpy as np
import scipy
from progressbar import ProgressBar


def angles_iterator(W, meas_mat, voxel_mask):
    """
    Splits Weight mat (the scan_gen.R_mat) and measurement to the respective angle scan
    :param W: Weight matrix (result of scan_gen.R_mat). shape - (n_angles x n_sensors,  n_voxels)
    :param meas_mat: Measurement matrix ( results of scan_gen.generate_clean/poisson or scan_gen.attenuation).
                    shape - (n_angles, n_sensors)
    :return: generator with yields: sub_w - weight matrix of a scan angle. shape - (n_sensors, n_voxels)
                                    sub_meas_mat - sensor matrix of a scan angle. shape - (n_sensors)
    """
    n_angles, n_sensors = meas_mat.shape
    print(n_angles, n_sensors)
    voxel_idx = np.flatnonzero(voxel_mask)
    bar = ProgressBar()
    for i_angle in bar(range(n_angles)):
        w_slice = np.ix_(np.arange(i_angle*n_sensors, (i_angle+1)*n_sensors), voxel_idx)
        sub_w = W[w_slice]
        sub_meas_mat = meas_mat[i_angle,:]
        yield sub_w, sub_meas_mat


def compute_crlb_coeff(weight_mat, measurement, rad_level, inv_flag=True):

    attenuation = measurement.flatten()
    sqrt_atten = np.sqrt(attenuation)

    if isinstance(weight_mat, scipy.sparse.csr.csr_matrix):
        delta_vec = weight_mat.T.multiply(sqrt_atten)
    else:
        delta_vec = (sqrt_atten*weight_mat.T).T

    fisher_info = (delta_vec @ delta_vec.T).todense()

    return np.linalg.inv(fisher_info) if inv_flag else fisher_info